#!/bin/bash

# Usage: sudo ./singularity_test.sh -t [<environment tag>]
# Or:    ./singularity_test.sh -f -t [<environment tag>]
# Either super user privilegies or the option 'fakeroot' are needed to build a Singularity image.
#
# environment tag: <package_name>/<version> (mandatory field)
# for example: AliPhysics/vAN-20201018_ROOT6-1
#
# /cvmfs/alice.cern.ch/bin and the availability of Singularity are checked as a prerequisite
#
# Rearranged from Serhat Atay's work (atay@compeng.uni-frankfurt.de).

TEMP=`getopt -o hft:s: --long help,fakeroot,tag:,sifdir: -n singularity_alice.sh -- "$@"`
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi
eval set -- "$TEMP"

help=false
clean=false
fakeroot=false
sifdir=$(pwd)

while true; do
    case "$1" in
        -h | --help ) help=true; shift ;;
        -f | --fakeroot ) fakeroot=true; shift ;;
        -t | --tag ) tag="$2"; shift 2 ;;
        -s | --sifdir ) sifdir="$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [[ "$help" = "true" ]]; then
    echo "options:"
    echo "-h, --help                        show brief help"
    echo "-f, --fakeroot                    build the Singularity image with --fakeroot"
    echo "-t, --tag <tag>                   specify a valid environment tag: mandatory!"
    echo "-s, --sifdir <dir>                specify a directory for the output image file"
    exit
fi

# Check if the environment tag is specified, if not exit.
if [ -z $tag ]; then
    echo "Please specify an environment tag using -t or --tag. Exiting."
    exit 1
fi

# Check if the ALICE cvmfs repository is mounted, if not exit.
if [[ ! -d /cvmfs/alice.cern.ch/bin ]]; then
    echo "/cvmfs/alice.cern.ch is not mounted. Exiting."
    exit 1
fi

# Check the "singularity" executable, if not found exit.
if ! command -v singularity &>/dev/null; then
    echo "Singularity is not installed on the system. Exiting."
    exit 1
fi

# Check that either sudo rights are available or the fakeroot flag has been given
if [[ $EUID > 0 ]]; then
    if ! [[ "$fakeroot" = "true" ]]; then
        echo "Building the Singularity image requires either to run the script with sudo or with the '--fakeroot' flag. Exiting ..."
        exit 1
    fi
fi

# Check that the output directory for the Singularity image exists, if not exit.
if ! [ -d ${sifdir} ]; then
    echo "The directory specified for the output Singularity image does not exist. Exiting."
    exit 1
fi

# $tag is the environment tag in the format <package_name>/<version>. We replace
# '/' with '-' to get $environment which we use for naming the definition and image files
environment=${tag/\//-}
def_file="${environment}.def"
sifdir=${sifdir%/}
output_sif="${sifdir}/${environment}.sif"

# Reproduce the needed libraries and modulefiles in the tarball (without intermediate copy)
# 1. get LD_LIBRARY_PATH from 'alienv printenv'
read -a allvars< <(/cvmfs/alice.cern.ch/bin/alienv printenv $tag)
for i in ${allvars[@]}; do if [[ "$i" == *"LD_LIBRARY_PATH="* ]]; then ld_path=$i; break; fi; done;
ld_path=${ld_path#*=}

# 2. loop to add LD_LIBRARY_PATH entries to the 'tar' command
delimiter=/Packages/
tarcmd="tar -czf cvmfs.tar.gz "
IFS=':'; for libdir in $ld_path; do
    # The following assumes libdirs of type /cvmfs/.../Packages/SoftwareName/version/lib
    if [[ "$libdir" == *"singularity"* ]] || ! [[ "$libdir" == *"Packages"* ]]; then
        continue
    fi
    versionpath=${libdir%/*}
    tarcmd="$tarcmd ${versionpath}"
done

# 3. Add modulefiles to the 'tar' command
for i in x86_64-2.6-gnu-4.1.2 el6-x86_64 el7-x86_64; do
    modulespath="/cvmfs/alice.cern.ch/$i/Modules"
    tarcmd="$tarcmd ${modulespath}"
done

# 4. add 'etc' and 'bin' to the tar command
tarcmd="$tarcmd /cvmfs/alice.cern.ch/etc /cvmfs/alice.cern.ch/bin"

# 5. create the tarball by evaluating the tar command. Later when extracting use '-h' to preserve symlinks
echo "Compressing the cvmfs content in a tarball ..."
eval $(echo $tarcmd)

# Create the Singularity definition file.
# It is like /cvmfs/alice.cern.ch/containers/fs/singularity/centos7/.singularity.d/Singularity
# with in addition the cvmfs tarball and its extraction.
echo "Creating the Singularity definition file ..."
cat > ${def_file} <<EOF
Bootstrap: docker
From: centos:centos7
IncludeCmd: no

%files
        cvmfs.tar.gz /

%post
  rpmdb --rebuilddb && yum clean all && rm -rf /var/cache/yum
        yum update -y
        yum -y  install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
        yum install -y attr autoconf automake avahi-compat-libdns_sd-devel bc bind-export-libs bind-libs bind-libs-lite bind-utils binutils bison bzip2-devel cmake compat-libgfortran-41 compat-libstdc++-33 e2fsprogs e2fsprogs-libs environment-modules fftw-devel file-devel flex gcc gcc-c++ gcc-gfortran git glew-devel glibc-devel glibc-static gmp-devel graphviz-devel java-1.7.0-openjdk libcurl-devel libpng-devel libtool libX11-devel libXext-devel libXft-devel libxml2-devel libxml2-static libXmu libXpm-devel libyaml-devel mesa-libGL-devel mesa-libGLU-devel motif-devel mpfr-devel mysql-devel ncurses-devel openldap-devel openssl-devel openssl-static pciutils-devel pcre-devel perl-ExtUtils-Embed perl-libwww-perl protobuf-devel python-devel readline-devel redhat-lsb rpm-build swig tcl tcsh texinfo tk-devel unzip uuid-devel wget which zip zlib-devel zlib-static zsh
        #yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
  #yum -y install cvmfs cvmfs-config-default
        yum clean all
        yum update -y
        tar -xphzf cvmfs.tar.gz -C /
        rm /cvmfs.tar.gz

%labels
Author Serhat Atay
EOF

# Finally build the image
echo "Building the Singularity image ..."
if [[ "$fakeroot" = "true" ]]; then
    singularity build --fakeroot ${output_sif} ${def_file}
else
    singularity build ${output_sif} ${def_file}
fi
