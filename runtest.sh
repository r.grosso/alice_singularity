#!/bin/bash
#SBATCH -o /lustre/rz/aliprod/cicdtest/ci_test.out
#SBATCH -e /lustre/rz/aliprod/cicdtest/ci_test.err
#SBATcH -D /tmp

set -e
singularity exec AliPhysics/vAN-20210531_ROOT6-1 /lustre/rz/aliprod/cicdtest/test.sh
