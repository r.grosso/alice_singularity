# Singularity container for ALICE environment

## Description
A shell script to fetch the binaries from the ALICE CVMFS repository for a specific environment tag and recreating them inside a Singularity image for later standalone use.

## Prerequisities
- **Singularity** is installed
- The ALICE CVMFS repository is mounted under _/cvmfs/alice.cern.ch_
- Either the script is run with sudo or with the '--fakeroot' flag for building the Singularity image

## Usage
- `git clone https://git.gsi.de/r.grosso/alice_singularity`
- `cd alice_singularity`
- `sudo ./singularity_alice.sh -t <environment_tag> -o <working_directory> -c`
-  or  `./singularity_alice.sh -t <environment_tag> -o <working_directory> -c -f`

The script accepts the following options:
- **-t**, **--tag**
    - specify a valid AliEn environment tag, for example *AliPhysics/vAN-20201018_ROOT6-1*
- **-s**, **--sifdir**
    - specify an output directory for the Singularity image file produced by the script. The default is the current directory.
- **-f**, **--fakeroot**
    - When building the Singularity image pass the option '--fakeroot' to build the image without being super user
- **-h**, **--help**
    - displays help

## Procedure
- The *alice_singularity.sh* script checks for arguments passed by the command. It exits with error:
    - if no environment tag is specified,
    - if alice.cern.ch cvmfs repository is not mounted,
    - if the directory specified as output directory for the Singularity image file does not exist
    - if Singularity is not installed or
    - if neither is being run with `sudo` nor with `--fakeroot`
- The tarball with the content extracted from CVMFS is built directly, without first copying into a scratch directory, to avoid generating many files on the local file system (probably Lustre)
- By means of `alienv printenv` the *LD_LIBRARY_PATH* is retrieved and decomposed in its component directories
- Each directory corresponding to a version of a given package in the *LD_LIBRARY_PATH* is added to the tar command
- Also the modules file are added to the tar command which is evaluated producing the tarball
- The definition file is used in the form of a here document and includes the cvmfs tarball created in the steps above and its untarring in the Singularity image

## Notes
- The Singularity image file can be found under **<working_directory>/<environment_tag>/<environment_tag>.sif**
- The image can be run with Singularity, e.g.:
    - **singularity run -B /<some_path> <image.sif>** or
    - **singularity exec -B /<some_path> <image.sif> /bin/bash**

:warning: Do not bind mount _/cvmfs/_ with "-B /cvmfs" as this would overlap with the _/cvmfs/_ repository already generated inside the image generating conflicts. Indeed if CVMFS is mounted there is no point in using this container.

- The Centos7 container only includes the binaries from the alice.cern.ch cvmfs repository for the environment tag specified to the building script.
- The environment in the container can be initialized as usual: "**/cvmfs/alice.cern.ch/bin/alienv enter <environment_tag>**" (and for that no cvmfs nor an internet connection is needed on the host).
- No additional data or analysis code is provided in the image.
